/*
 * io_addresses.h
 *
 *  Created on: 11.12.2013
 *      Author: Philip Urban
 */

#ifndef IO_ADDRESSES_H_
#define IO_ADDRESSES_H_

#define TX_FREQUENCY_1_ADR 0
#define TX_VOLTAGE_1_ADR 12
#define TX_VOLTAGE_2_ADR 24
#define TX_CURRENT_1_ADR 36
#define TX_CURRENT_2_ADR 48
#define RX_ANALOG_1_ADR 0
#define RX_ANALOG_2_ADR 12

#endif /* IO_ADDRESSES_H_ */
