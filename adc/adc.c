/*
 * adc.c
 *
 *  Created on: 11.12.2013
 *      Author: Philip Urban
 */

#include <avr/interrupt.h>
#include <avr/io.h>
#include "adc.h"


void init_adc() {

	// initialize pins
	DDRC &= ~(1<<DDC0 | 1<<DDC1 | 1<<DDC2 | 1<<DDC3);
}

void adc_get_channel(uint8_t channel, uint16_t* value) {

	uint16_t result;
	ADMUX = channel;		// channel selection
	ADMUX  |= (1<<REFS0);	// external reference voltage
	ADCSRA |= (1<<ADEN);	// enable converter
	ADCSRA |= (1<<ADPS2) | (1<<ADPS1);	// prescaler 8
	ADCSRA |= (1<<ADSC);	// start conversation

	while (ADCSRA & (1<<ADSC)) {}
	result = ADCW;

	ADCSRA &= ~(1<<ADEN);	// disable ADC
	*value = result;
}

void adc_get_voltage(uint8_t channel, uint16_t *voltage) {

	uint32_t buffer;
	uint16_t adc;
	buffer = 1000000 / 1023;
	adc_get_channel(channel, &adc);
	buffer *= adc;
	buffer /= 100;
	*voltage = buffer;
}

void adc_get_current(uint8_t channel, uint16_t *current) {

	uint32_t buffer;
	uint16_t adc;
	buffer = 500000 / 1023;
	adc_get_channel(channel, &adc);
	buffer *= adc;
	buffer /= 250;
	*current = buffer;
}
