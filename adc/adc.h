/*
 * adc.h
 *
 *  Created on: 11.12.2013
 *      Author: Philip Urban
 */

#ifndef ADC_H_
#define ADC_H_

void init_adc();
void adc_get_voltage(uint8_t channel, uint16_t *voltage);
void adc_get_current(uint8_t channel, uint16_t *current);

#endif /* ADC_H_ */
