/*
 * frequency.h
 *
 *  Created on: 11.12.2013
 *      Author: Philip Urban
 */

#ifndef FREQUENCY_H_
#define FREQUENCY_H_

extern volatile uint8_t frequencyUpdateAvailable;

void init_frequency_measurement();

void frequency_get(uint32_t *buffer);
void frequency_reset();

#endif /* FREQUENCY_H_ */
