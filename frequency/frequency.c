/*
 * frequency.c
 *
 *  Created on: 11.12.2013
 *      Author: Philip Urban
 */

#include <avr/interrupt.h>
#include "frequency.h"

volatile uint8_t overflowCount = 0;
volatile uint16_t  startTime = 0;
volatile uint16_t  endTime = 0;
volatile uint8_t frequencyUpdateAvailable;
volatile uint8_t firstEdge = 1;

ISR( TIMER1_CAPT_vect )
{
	if(frequencyUpdateAvailable) return;

	if(firstEdge) {
		startTime = ICR1;
		overflowCount = 0;
		firstEdge = 0;
	}
	else {
		endTime = ICR1;
		frequencyUpdateAvailable = 1;
		firstEdge = 1;
	}
}

ISR( TIMER1_OVF_vect )
{
	overflowCount++;
}

void init_frequency_measurement() {

	TCCR1B = (1<<ICES1)  | (1<<CS11) | (1<<CS10); // input capture edge, prescale
	TIMSK = (1<<TICIE1) | (1<<TOIE1); // interrupts: capture + overflow
	DDRB &= ~(1<<DDB0);
}

void frequency_get(uint32_t *buffer) {

	frequencyUpdateAvailable = 0;
	*buffer = (overflowCount * 65536) + endTime - startTime;
	*buffer = (F_CPU * 10 / *buffer) / 64; // last number is prescale factor
}

void frequency_reset() {

	firstEdge = 1;
	overflowCount = 0;
	startTime = 0;
	endTime = 0;
}
