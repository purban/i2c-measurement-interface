/*
 * main.c
 *
 *  Created on: 09.12.2013
 *      Author: Philip Urban
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "twi/twislave.h"
#include "frequency/frequency.h"
#include "adc/adc.h"
#include "dac/dac.h"
#include "io_addresses.h"

#define ever ;;
#define I2C_ADR 4

uint32_t updateCount = 0;
uint32_t frequencyZeroCount = 0;
uint32_t frequency = 0;

int main(void) {

	//init_frequency_measurement(); // not working if DAC 1 is used (same timer)
	init_adc();
	init_dac();
	init_twi_slave(I2C_ADR);

	sei();

	// shutdown button
	DDRD &= ~(1<<DDD7);

	// status led
	DDRD |= (1<<DDD6);
	PORTD |= (1<<PIND6);

	uint16_t uint16_temp;
	uint32_t uint32_temp;

	for(ever) {

		updateCount ++;
		// slow interval for time intensive measurements
		if(updateCount == 50000) {
			updateCount = 0;

			// transmit adc voltage
			adc_get_voltage(0, &uint16_temp);
			setValue(TX_VOLTAGE_1_ADR, uint16_temp);

			adc_get_voltage(1, &uint16_temp);
			setValue(TX_VOLTAGE_2_ADR, uint16_temp);

			adc_get_current(2, &uint16_temp);
			setValue(TX_CURRENT_1_ADR, uint16_temp);

			adc_get_current(3, &uint16_temp);
			setValue(TX_CURRENT_2_ADR, uint16_temp);

			getValue(RX_ANALOG_1_ADR, &uint16_temp);
			dac_set_voltage(0, uint16_temp);

			getValue(RX_ANALOG_2_ADR, &uint16_temp);
			dac_set_voltage(1, uint16_temp);
		}

		// check if shutdown is initiated
		if(PIND & (1<<PIND7)) {

			// blink led a while then turn off
			uint8_t blinkCount = 0;
			updateCount = 0;
			for(ever) {
				updateCount++;
				if(updateCount == 200000) {
					updateCount = 0;
					PORTD ^= (1<<PIND6);
					blinkCount++;
				}

				if(blinkCount == 50) {
					PORTD &= ~(1<<PIND6);
					break;
				}
			}
		}

//		if(frequencyUpdateAvailable) {
//			frequencyZeroCount = 0;
//			frequency_get(&frequency);
//			setValue(TX_FREQUENCY_1_ADR, frequency);
//		}
//
//		frequencyZeroCount++;
//		if(frequencyZeroCount >= 1000000) {
//			setValue(TX_FREQUENCY_1_ADR, 0);
//			frequency_reset();
//			frequencyZeroCount = 0;
//		}
	}

	return 0;
}
