/*
 * dac.c
 *
 *  Created on: 12.12.2013
 *      Author: Philip Urban
 */

#include <avr/interrupt.h>
#include <avr/io.h>
#include "dac.h"

void init_dac() {

	DDRB |= (1<<DDB1); // output pin DAC1
	DDRB |= (1<<DDB3); // output pin DAC2

	// DAC1 -> Timer 1
	TCCR1A |= (1<<WGM10); 				// pwm, 8bit
	TCCR1A |= (1<<COM1A1); 				//
	TCCR1B = (1<<CS10); 				// no prescaler

	// DAC2 -> Timer 2
	TCCR2 |= (1<<WGM20) | (1<<WGM21); 	// fast pwm mode
	TCCR2 |= (1<<COM21); 				// non inverting mode
	TCCR2 |= (1<<CS20);					// no prescaler
}

void dac_set_voltage(uint8_t channel, uint16_t milliVolt) {

	uint16_t buffer;
	buffer = milliVolt / (10000 / 255);
	if(buffer > 1) buffer -= 1;
	if(buffer > 255) buffer = 255;

	switch(channel) {
		case 0: OCR1A = buffer; break;
		case 1: OCR2 = buffer; break;
	}
}
