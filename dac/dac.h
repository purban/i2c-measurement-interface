/*
 * dac.h
 *
 *  Created on: 12.12.2013
 *      Author: Philip Urban
 */

#ifndef DAC_H_
#define DAC_H_

void init_dac();
void dac_set_voltage(uint8_t channel, uint16_t milliVolt);

#endif /* DAC_H_ */
